﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using MVC_Curso.DTOs;
using MVC_Curso.Models;

namespace MVC_Curso.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Domain => DTO
            Mapper.CreateMap<Customer, CustomerDTO>();
            Mapper.CreateMap<Movie, MoviesDTO>();
            Mapper.CreateMap<MembershipType, MembershipTypeDTO>();
            Mapper.CreateMap<Genre, GenreDTO>();

            //DTO => Domain
            Mapper.CreateMap<CustomerDTO, Customer>()
                .ForMember(c => c.Id, opt => opt.Ignore());

            Mapper.CreateMap<MoviesDTO, Movie>()
                .ForMember(c => c.Id, opt => opt.Ignore());
        }
    }
}