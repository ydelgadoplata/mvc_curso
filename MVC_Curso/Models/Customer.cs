﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace MVC_Curso.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        public bool IsSubscirbedToNewsLetter { get; set; }

        public MembershipType MembershipType { get; set; }

        [Display(Name = "Membresía")]
        public byte MembershipTypeId { get; set; }

        [Display(Name = "Fecha de Nacimiento")] //Se debe compilar al agregar esta opcion para mostrar el label
        [Min18YearsIfAMember]
        public DateTime? BirthDate { get; set; }
    }
}

