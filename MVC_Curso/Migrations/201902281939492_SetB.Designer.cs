// <auto-generated />
namespace MVC_Curso.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SetB : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SetB));
        
        string IMigrationMetadata.Id
        {
            get { return "201902281939492_SetB"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
