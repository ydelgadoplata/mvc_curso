namespace MVC_Curso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetB : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Customers SET birthDate = '10/05/1982' WHERE Id = 2");
            Sql("UPDATE Customers SET birthDate = '01/09/2017' WHERE Id = 4");
        }

        public override void Down()
        {
        }
    }
}
