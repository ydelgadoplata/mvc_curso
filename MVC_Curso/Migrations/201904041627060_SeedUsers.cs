namespace MVC_Curso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'704820d6-d0de-4b70-a8f4-063fcd324804', N'admin@mvccourse.com', 0, N'AJcmu0/hpzdyhuTJwq4YqHuneQDqMveteKzBJDboXE21PxNqj+imUhI3wToY0l1f5g==', N'a52d87fa-ddf9-4431-a0a6-665f14f2d2ab', NULL, 0, 0, NULL, 1, 0, N'admin@mvccourse.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fce66236-2117-4052-8a82-c71ca9f1f05c', N'guest@mvccourse.com', 0, N'AKm/ZgVA45IRJHK+PPkoKnoK/8zjvQBaolKCkfeJeqT7TKbSM8HhOuhpXGAa9QE4mw==', N'a6d6f8a7-01e4-4e5c-bc9b-33dbe314dacf', NULL, 0, 0, NULL, 1, 0, N'guest@mvccurse.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'9676cf6e-1f73-4cd3-bbd7-a4c69c4149a4', N'CanManageMovie')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'704820d6-d0de-4b70-a8f4-063fcd324804', N'9676cf6e-1f73-4cd3-bbd7-a4c69c4149a4')

");
        }
        
        public override void Down()
        {
        }
    }
}
