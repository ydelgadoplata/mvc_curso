﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Curso.Models;

namespace MVC_Curso.ViewModels
{
    public class RandomMovieViewModel
    {
        public Movie Movie { get; set; }
        public List<Customer> Customers { get; set; }
    }
}